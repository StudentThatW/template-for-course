migrate:
	docker-compose run rest python src/manage.py migrate --noinput

makemigrations:
	python src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	docker-compose run rest python src/manage.py createsuperuser

collectstatic:
	docker-compose run rest python src/manage.py collectstatic --no-input

dev:
	docker-compose run rest python src/manage.py runserver localhost:8000

command:
	docker-compose run rest python src/manage.py ${c}

shell:
	docker-compose run rest python src/manage.py shell

debug:
	docker-compose run rest python src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

test:
	docker-compose run rest pytest src/tests

lint:
	isort -rc .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint_docker:
	docker-compose run --rm rest sh -c "isort --check --diff . && flake8 --config setup.cfg"

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

dockerstart:
	docker-compose up --build -d

build:
	docker-compose build

up:
	docker-compose up -d

down:
	docker-compose down

