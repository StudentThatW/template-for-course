FROM python:3

ENV PYTHONUNBUFFERED=1

WORKDIR /usr/src/app

COPY Pipfile ./
COPY Pipfile.lock ./

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.9.0/wait /wait
RUN chmod +x /wait

RUN pip3 install pipenv
RUN pip3 install pytest-django
RUN pip3 install pyjwt
RUN pip3 install django-ninja
RUN pipenv install --system --deploy

COPY . .

CMD ["pipenv", "run", "python3", "src/manage.py", "runserver", "pytest"]
