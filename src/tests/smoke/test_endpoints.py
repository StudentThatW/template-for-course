import json

import pytest
from django.http import HttpResponse
from django.urls import get_resolver, reverse

from app.internal.authentification.db.repositories import IssuedTokenRepository
from app.internal.authentification.domain.services import IssuedTokenService
from app.internal.favourite_users.db.repositories import FavouriteTgUsersRepository
from app.internal.favourite_users.domain.services import FavouriteTgUsersService
from app.internal.tg_users.db.models import TgUser


def get_auth_header(test_user: TgUser):
    iss_repo = IssuedTokenRepository()
    iss_service = IssuedTokenService(iss_repo)
    access_token = iss_service.generate_access_token(test_user)
    return {"HTTP_AUTHORIZATION": f"Bearer {access_token}"}


@pytest.mark.django_db
def test_get_fav_router(client, test_user, test_user_without_phone):
    fav_rep = FavouriteTgUsersRepository()
    fav_service = FavouriteTgUsersService(fav_rep)
    fav_service.add_favorite_tguser(test_user, test_user_without_phone)
    url = "/api/special_api/favourite_users/5/list"
    header = get_auth_header(test_user)
    response: HttpResponse = client.get(url, **header)

    content = json.loads(response.content)
    assert response.status_code == 200
    assert content[0]["favorite"] == 6
    assert content[0]["possessor"] == 5


@pytest.mark.django_db
def test_get_fav_router_exception(client, test_user, test_user_without_phone):
    fav_rep = FavouriteTgUsersRepository()
    fav_service = FavouriteTgUsersService(fav_rep)
    fav_service.add_favorite_tguser(test_user, test_user_without_phone)

    url = "/api/special_api/favourite_users/2/list"
    header = get_auth_header(test_user)
    response: HttpResponse = client.get(url, **header)

    content = json.loads(response.content)
    assert response.status_code == 400
    assert content["message"] == "No user with id 2"


@pytest.mark.django_db
def test_del_fav_router(client, test_user, test_user_without_phone):
    fav_rep = FavouriteTgUsersRepository()
    fav_service = FavouriteTgUsersService(fav_rep)
    fav_service.add_favorite_tguser(test_user, test_user_without_phone)

    _data = b'{"possessor_id":"5","favourite_id":"6"}'
    url = "/api/special_api/favourite_users/delete"
    header = get_auth_header(test_user)
    response: HttpResponse = client.delete(url, **header, data=_data)
    content = json.loads(response.content)
    assert response.status_code == 200
    assert content["success"] is True


@pytest.mark.django_db
def test_del_fav_router_exception(client, test_user, test_user_without_phone):
    _data = b'{"possessor_id":"5","favourite_id":"6"}'
    url = "/api/special_api/favourite_users/delete"
    header = get_auth_header(test_user)
    response: HttpResponse = client.delete(url, **header, data=_data)
    content = json.loads(response.content)
    assert response.status_code == 400
    assert content["message"] == "User was not favourite"


@pytest.mark.django_db
def test_post_login(client, test_user_with_password):
    iss_repo = IssuedTokenRepository()
    post_data = {'login': '14', 'password': 'dez'}
    header = get_auth_header(test_user_with_password)
    response: HttpResponse = client.post("/api/special_api/authentication/login",
                                         ** header, data=post_data, content_type='application/json')
    content = json.loads(response.content)

    assert response.status_code == 201
    access_token = content["access_token"]
    access_payload = iss_repo.decode_token(access_token)
    assert access_payload["tguser_id"] == "14"
    assert access_payload["type"] == "access"


@pytest.mark.django_db
def test_post_login_exception(client, test_user_with_password):
    post_data = {'login': '15', 'password': 'dez'}
    header = get_auth_header(test_user_with_password)
    response: HttpResponse = client.post("/api/special_api/authentication/login",
                                         ** header, data=post_data, content_type='application/json')
    content = json.loads(response.content)

    assert response.status_code == 401
    assert content["message"] == "No user with id 15"


@pytest.mark.django_db
def test_post_refresh(client, test_user_with_password):
    iss_repo = IssuedTokenRepository()
    refresh_token = iss_repo.generate_refresh_token(test_user_with_password)
    post_data = {'jti': refresh_token}
    header = get_auth_header(test_user_with_password)
    response: HttpResponse = client.post("/api/special_api/authentication/refresh",
                                         **header, data=post_data, content_type='application/json')
    content = json.loads(response.content)
    assert response.status_code == 201
    access_token = content["access_token"]
    access_payload = iss_repo.decode_token(access_token)
    assert access_payload["tguser_id"] == "14"
    assert access_payload["type"] == "access"


@pytest.mark.django_db
def test_post_refresh_exception(client, test_user_with_password):
    iss_repo = IssuedTokenRepository()
    iss_service = IssuedTokenService(iss_repo)
    refresh_token_str = iss_service.generate_refresh_token(test_user_with_password)
    iss_service.revoke_refresh_token(iss_service.get_refresh_token(refresh_token_str))
    post_data = {'jti': refresh_token_str}
    header = get_auth_header(test_user_with_password)
    response: HttpResponse = client.post("/api/special_api/authentication/refresh",
                                         **header, data=post_data, content_type='application/json')
    content = json.loads(response.content)
    assert response.status_code == 400
    assert content["message"] == "Token revoked"


@pytest.mark.django_db
def test_get_me(client, test_user):
    header = get_auth_header(test_user)
    response: HttpResponse = client.get("/api/special_api/tg_users/5/me", **header)

    content = json.loads(response.content)
    assert response.status_code == 200
    assert content["telegram_id"] == 5


@pytest.mark.django_db
def test_put_phone(client, test_user):
    header = get_auth_header(test_user)
    put_data = {'phone_number': '+7912452462426'}

    response: HttpResponse = client.put("/api/special_api/tg_users/5/phone", **header,
                                        data=put_data, content_type='application/json')

    content = json.loads(response.content)
    assert response.status_code == 200
    assert content["success"] is True


@pytest.mark.django_db
def test_get_me_exception(client, test_user):
    header = get_auth_header(test_user)
    response: HttpResponse = client.get("/api/special_api/tg_users/6/me", **header)

    content = json.loads(response.content)
    assert response.status_code == 400
    assert content["message"] == "No user with id 6"


@pytest.mark.django_db
def test_put_phone_exception(client, test_user):
    header = get_auth_header(test_user)
    put_data = {'phone_number': '+7912452462426'}

    response: HttpResponse = client.put("/api/special_api/tg_users/6/phone", **header,
                                        data=put_data, content_type='application/json')

    content = json.loads(response.content)
    assert response.status_code == 400
    assert content["message"] == "No user with id 6"
