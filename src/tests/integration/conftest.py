from unittest.mock import MagicMock

import pytest

from app.internal.bank_account_and_card.db.models import BankAccount, Card
from app.internal.favourite_users.db.models import FavouriteTgUser
from app.internal.tg_users.db.models import TgUser


@pytest.fixture(scope="function")
def mocked_update() -> MagicMock:
    mocked_message = MagicMock()
    mocked_message.reply_text = MagicMock()

    mocked_user = MagicMock()
    mocked_user.id = 123456
    mocked_user.username = "username"
    mocked_user.first_name = "first_name"

    mock = MagicMock()
    mock.message = mocked_message
    mock.effective_user = mocked_user
    return mock


@pytest.fixture(scope="function")
def mocked_context() -> MagicMock:
    mock = MagicMock()
    return mock


@pytest.fixture(scope="function")
def test_user_with_account_and_card_and_same_id():
    bank_user = TgUser.objects.create(
        telegram_id=123456, first_name="bank_first_name", username="bank_username", phone_number="777777777777"
    )
    bank_account1 = BankAccount.objects.create(id=5, tg_user=bank_user , amount=1000)
    card1 = Card.objects.create(number=5, bank_account=bank_account1)
    return bank_user, bank_account1, card1


@pytest.fixture(scope="function")
def test_user_with_account_and_card_and_different_id():
    bank_user2 = TgUser.objects.create(
        telegram_id=123458, first_name="bank_first_name2", username="Bank_username2", phone_number="788888888888"
    )
    bank_account2 = BankAccount.objects.create(id=6, tg_user=bank_user2 , amount=1000)
    card2 = Card.objects.create(number=6, bank_account=bank_account2)
    return bank_user2, bank_account2, card2


@pytest.fixture(scope="function")
def test_favourite_user():
    owner_user = TgUser.objects.create(
        telegram_id=123456, first_name="owner_user", username="owner_user", phone_number="79999999994"
    )
    user_to_favourite = TgUser.objects.create(
        telegram_id=123460, first_name="user_to_favourite", username="user_to_favourite", phone_number="79999999995"
    )
    favourite_user = FavouriteTgUser.objects.update_or_create(possessor=owner_user, favorite=user_to_favourite)
    return owner_user, user_to_favourite, favourite_user
