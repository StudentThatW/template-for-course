from unittest.mock import MagicMock

import pytest

from app.internal.bank_account_and_card.db.models import BankAccount, Card, MoneyTransaction
from app.internal.favourite_users.db.models import FavouriteTgUser
from app.internal.responses.responses_strings import (
    INVALID_ARGUMENTS_CARD_AMOUNT_REPLY,
    INVALID_ARGUMENTS_GET_ACCOUNT_AMOUNT_REPLY,
    INVALID_ARGUMENTS_TRANSFER_AMOUNT_REPLY,
    MONEY_TRANSFERED_REPLY,
    NO_RIGHTS_FOR_BANK_REPLY,
    PHONE_NUMBER_NOT_SAVED_REPLY,
    USER_NOT_FAVOURITE_REPLY,
)


@pytest.mark.integration
class TestMoveMoneyToUser:
    @pytest.mark.parametrize("test_input,expected", [(["5"], "1000"), ([], INVALID_ARGUMENTS_GET_ACCOUNT_AMOUNT_REPLY)])
    @pytest.mark.django_db
    def test_account_handler(self, mocked_update: MagicMock, mocked_context: MagicMock, test_user_with_account_and_card_and_same_id,
                             test_input, expected, test_bank_handler):
        mocked_context.args = test_input
        test_bank_handler.get_account_amount(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(expected)

    @pytest.mark.parametrize("test_input,expected", [(["5"], "1000"), ([], INVALID_ARGUMENTS_CARD_AMOUNT_REPLY)])
    @pytest.mark.django_db
    def test_card_handler(self, mocked_update: MagicMock, mocked_context: MagicMock, test_user_with_account_and_card_and_same_id,
                          test_input, expected, test_bank_handler):
        mocked_context.args = test_input

        test_bank_handler.card_amount(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(expected)

    @pytest.mark.django_db
    def test_account_handler_not_own(self, mocked_update: MagicMock, mocked_context: MagicMock,
                                     test_user_with_account_and_card_and_different_id,
                                     test_user_with_account_and_card_and_same_id,
                                     test_bank_handler):
        mocked_context.args = ["6"]
        test_bank_handler.get_account_amount(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(NO_RIGHTS_FOR_BANK_REPLY)

    @pytest.mark.django_db
    def test_card_handler_not_own(self, mocked_update: MagicMock, mocked_context: MagicMock,
                                  test_user_with_account_and_card_and_different_id,
                                  test_user_with_account_and_card_and_same_id,
                                  test_bank_handler):
        mocked_context.args = ["6"]
        test_bank_handler.card_amount(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(NO_RIGHTS_FOR_BANK_REPLY)

    @pytest.mark.django_db
    def test_transfer_handler_without_favourite(self, mocked_update: MagicMock, mocked_context: MagicMock,
                                                test_user_with_account_and_card_and_same_id,
                                                test_user_with_account_and_card_and_different_id,
                                                test_bank_handler):
        mocked_context.args = ["5", "account", "6", "50"]

        test_bank_handler.transfer_amount(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(USER_NOT_FAVOURITE_REPLY)

    @pytest.mark.parametrize("correct_args", [["5", "account", "6", "50"], ["5", "card", "6", "50"], ["5", "username", "Bank_username2", "50"]])
    @pytest.mark.django_db
    def test_transfer_handler_with_favourite(self, mocked_update: MagicMock, mocked_context: MagicMock,
                                             test_user_with_account_and_card_and_same_id,
                                             test_user_with_account_and_card_and_different_id, correct_args,
                                             test_bank_handler):
        bank_user1 = test_user_with_account_and_card_and_same_id[0]
        bank_user2 = test_user_with_account_and_card_and_different_id[0]
        FavouriteTgUser.objects.update_or_create(possessor=bank_user1, favorite=bank_user2)
        mocked_context.args = correct_args

        test_bank_handler.transfer_amount(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(
            MONEY_TRANSFERED_REPLY.format(account_id="5", amount="950"))

    @pytest.mark.parametrize("incorrect_args", [["account", "6", "50"], [], ["5", "accountt", "6", "50"], ["5", "accountt", "account_id", "50"],
                                                ["5", "account", "6", "-50"]])
    @pytest.mark.django_db
    def test_transfer_handler_without_arguments(self, mocked_update: MagicMock, mocked_context: MagicMock,
                                                test_user_with_account_and_card_and_same_id,
                                                test_user_with_account_and_card_and_different_id, incorrect_args,
                                                test_bank_handler):
        bank_user1 = test_user_with_account_and_card_and_same_id[0]
        bank_user2 = test_user_with_account_and_card_and_different_id[0]
        FavouriteTgUser.objects.update_or_create(possessor=bank_user1, favorite=bank_user2)
        mocked_context.args = incorrect_args

        test_bank_handler.transfer_amount(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(INVALID_ARGUMENTS_TRANSFER_AMOUNT_REPLY)

    @pytest.mark.django_db
    def test_interactions_handler(self, mocked_update: MagicMock, mocked_context: MagicMock,
                                  test_user_with_account_and_card_and_same_id,
                                  test_user_with_account_and_card_and_different_id,
                                  test_bank_handler):
        bank_user1 = test_user_with_account_and_card_and_same_id[0]
        bank_user2 = test_user_with_account_and_card_and_different_id[0]
        FavouriteTgUser.objects.update_or_create(possessor=bank_user1, favorite=bank_user2)
        mocked_context.args = ["5", "account", "6", "50"]

        test_bank_handler.transfer_amount(mocked_update, mocked_context)
        mocked_context.args = []
        test_bank_handler.get_interactions(mocked_update, mocked_context)
        interactions_list = [bank_user2.username]
        reply = test_bank_handler.get_interactions_relpy_text(interactions_list)
        mocked_update.message.reply_text.assert_called_with(reply)

    @pytest.mark.django_db
    def test_transactions_handler(self, mocked_update: MagicMock, mocked_context: MagicMock, test_user_with_account_and_card_and_same_id,
                                  test_user_with_account_and_card_and_different_id, test_bank_handler):
        bank_user1 = test_user_with_account_and_card_and_same_id[0]
        bank_user2 = test_user_with_account_and_card_and_different_id[0]
        FavouriteTgUser.objects.update_or_create(possessor=bank_user1, favorite=bank_user2)
        mocked_context.args = ["5", "account", "6", "50"]

        test_bank_handler.transfer_amount(mocked_update, mocked_context)
        mocked_context.args = ["5"]
        test_bank_handler.get_transactions(mocked_update, mocked_context)
        answer = test_bank_handler.get_transactions_relpy_text("5", MoneyTransaction.objects.all())
        mocked_update.message.reply_text.assert_called_with(answer)

    @pytest.mark.django_db
    def test_if_no_phone_bank_account(self, mocked_update: MagicMock, mocked_context: MagicMock, test_user_without_phone, test_bank_handler):
        test_bank_handler.get_account_amount(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(PHONE_NUMBER_NOT_SAVED_REPLY)

    @pytest.mark.django_db
    def test_if_no_phone_bank_card(self, mocked_update: MagicMock, mocked_context: MagicMock, test_user_without_phone, test_bank_handler):
        test_bank_handler.card_amount(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(PHONE_NUMBER_NOT_SAVED_REPLY)

    @pytest.mark.django_db
    def test_if_no_phone_bank_get_transactions(self, mocked_update: MagicMock, mocked_context: MagicMock, test_user_without_phone, test_bank_handler):
        test_bank_handler.get_transactions(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(PHONE_NUMBER_NOT_SAVED_REPLY)
