from unittest.mock import MagicMock

import pytest

from app.internal.responses.responses_strings import (
    INVALID_ARGUMENTS_CARD_AMOUNT_REPLY,
    INVALID_ARGUMENTS_GET_ACCOUNT_AMOUNT_REPLY,
    INVALID_ARGUMENTS_SET_PHONE_REPLY,
    INVALID_ARGUMENTS_TRANSFER_AMOUNT_REPLY,
    NO_RIGHTS_FOR_BANK_REPLY,
    PHONE_NUMBER_NOT_SAVED_REPLY,
    PHONE_NUMBER_SAVED_REPLY,
    START_HELLO_REPLY,
)
from app.internal.tg_users.db.models import TgUser


@pytest.mark.integration
class TestStartCommands:
    @pytest.mark.django_db
    def test_start_handler(self, mocked_update: MagicMock, mocked_context: MagicMock, test_tguser_bot_handler):
        test_tguser_bot_handler.start(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with("Hello, first_name")
        assert TgUser.objects.filter(telegram_id=123456).exists()

    @pytest.mark.django_db
    def test_set_phone_handler_valid(self, mocked_update: MagicMock, mocked_context: MagicMock, test_tguser_bot_handler):
        mocked_context.args = ["+7922222222"]
        test_tguser_bot_handler.set_phone(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(PHONE_NUMBER_SAVED_REPLY)
        assert str(TgUser.objects.get(telegram_id=mocked_update.effective_user.id).phone_number) == mocked_context.args[0]

    @pytest.mark.django_db
    def test_set_phone_handler_no_args(self, mocked_update: MagicMock, mocked_context: MagicMock, test_tguser_bot_handler):
        test_tguser_bot_handler.set_phone(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(INVALID_ARGUMENTS_SET_PHONE_REPLY)

    @pytest.mark.django_db
    def test_set_phone_handler_invalid_format(self, mocked_update: MagicMock, mocked_context: MagicMock, test_tguser_bot_handler):
        mocked_context.args = ["1"]
        test_tguser_bot_handler.set_phone(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(INVALID_ARGUMENTS_SET_PHONE_REPLY)

    @pytest.mark.django_db
    def test_me_handler(self, mocked_update: MagicMock, mocked_context: MagicMock, test_tguser_bot_handler):
        test_tguser_bot_handler.start(mocked_update, mocked_context)
        mocked_context.args = ["+7922222222"]
        test_tguser_bot_handler.set_phone(mocked_update, mocked_context)
        mocked_context.args = []
        test_tguser_bot_handler.me(mocked_update, mocked_context)
        answer = '{\n  "username": "username",\n  "first_name": "first_name",\n  "telegram_id": 123456,\n  "phone number": "+7922222222"\n}'
        mocked_update.message.reply_text.assert_called_with(answer)

    @pytest.mark.django_db
    def test_me_handler_no_phone(self, mocked_update: MagicMock, mocked_context: MagicMock, test_tguser_bot_handler):
        test_tguser_bot_handler.start(mocked_update, mocked_context)
        test_tguser_bot_handler.me(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_with(PHONE_NUMBER_NOT_SAVED_REPLY)
