from unittest.mock import MagicMock

import pytest

from app.internal.responses.responses_strings import (
    FAVOURITE_USERS_START_REPLY,
    PHONE_NUMBER_NOT_SAVED_REPLY,
    USER_ADDED_TO_FAVOURITES,
    USER_DELETED_FROM_FAVOURITES,
)


@pytest.mark.integration
class TestFavouriteUsersHandlers:
    @pytest.mark.django_db
    def test_add_favourite(self, mocked_update: MagicMock, mocked_context: MagicMock, test_user_with_account_and_card_and_same_id, test_user_without_phone,
                           test_favourite_tguser_bot_handler):
        mocked_context.args = ["6"]
        test_favourite_tguser_bot_handler.add_favorite(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(USER_ADDED_TO_FAVOURITES)

    @pytest.mark.django_db
    def test_delete_favourite(self, mocked_update: MagicMock, mocked_context: MagicMock, test_favourite_user,
                              test_favourite_tguser_bot_handler):
        mocked_context.args = ["123460"]
        test_favourite_tguser_bot_handler.remove_favorite(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(USER_DELETED_FROM_FAVOURITES)

    @pytest.mark.django_db
    def test_favourite_list(self, mocked_update: MagicMock, mocked_context: MagicMock, test_favourite_user,
                            test_favourite_tguser_bot_handler):
        test_favourite_tguser_bot_handler.get_favorite(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(FAVOURITE_USERS_START_REPLY + "1. 123460\n")

    @pytest.mark.django_db
    def test_if_no_phone_favourite_add(self, mocked_update: MagicMock, mocked_context: MagicMock, test_favourite_tguser_bot_handler):
        test_favourite_tguser_bot_handler.add_favorite(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(PHONE_NUMBER_NOT_SAVED_REPLY)

    @pytest.mark.django_db
    def test_if_no_phone_favourite_get(self, mocked_update: MagicMock, mocked_context: MagicMock, test_favourite_tguser_bot_handler):
        test_favourite_tguser_bot_handler.get_favorite(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(PHONE_NUMBER_NOT_SAVED_REPLY)

    @pytest.mark.django_db
    def test_if_no_phone_favourite_remove(self, mocked_update: MagicMock, mocked_context: MagicMock, test_favourite_tguser_bot_handler):
        test_favourite_tguser_bot_handler.remove_favorite(mocked_update, mocked_context)
        mocked_update.message.reply_text.assert_called_once_with(PHONE_NUMBER_NOT_SAVED_REPLY)
