import pytest


@pytest.mark.unit
class TestUserService:
    @pytest.mark.django_db
    def test_create_tguser(self, test_tguser_service):
        test_tguser_service.create_user(3, "Dan", "dan")
        actual = test_tguser_service.get_user(telegram_id="3")
        assert actual.first_name == "Dan"

    @pytest.mark.django_db
    def test_get_tguser_by_telegram_id(self, test_user, test_tguser_service):
        actual = test_tguser_service.get_user(telegram_id=test_user.telegram_id)
        assert actual == test_user

    @pytest.mark.django_db
    def test_set_tguser_phone(self, test_user, test_tguser_service):
        phone_number = "79333333333"
        test_tguser_service.set_user_phone(telegram_id=test_user.telegram_id, phone=phone_number)
        current_test_user = test_tguser_service.get_user(telegram_id=test_user.telegram_id)
        assert current_test_user.phone_number == phone_number
