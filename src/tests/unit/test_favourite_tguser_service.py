import pytest


@pytest.mark.unit
class TestFavouriteUsersService:
    @pytest.mark.django_db
    def test_add_to_favorite(self, test_user, test_user_without_phone, test_favourite_tguser_service):
        test_favourite_tguser_service.add_favorite_tguser(test_user, test_user_without_phone)
        favorites = test_favourite_tguser_service.get_favourite_tgusers(test_user)
        assert favorites[0].favorite == test_user_without_phone

    @pytest.mark.django_db
    def test_remove_from_favorite(self, test_user_with_favourite_user, test_favourite_tguser_service):
        user1, user2 = test_user_with_favourite_user
        test_favourite_tguser_service.add_favorite_tguser(user1, user2)
        delete = test_favourite_tguser_service.remove_favorite_tguser(user1, user2)
        assert delete is None
        favorites = test_favourite_tguser_service.get_favourite_tgusers(user1)
        assert not favorites

    @pytest.mark.django_db
    def test_remove_from_favorite_exception(self, test_user_with_favourite_user, test_favourite_tguser_service):
        user1, user2 = test_user_with_favourite_user
        delete = test_favourite_tguser_service.remove_favorite_tguser(user1, user2)
        assert delete == "User was not favourite"
