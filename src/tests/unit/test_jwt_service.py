import pytest


@pytest.mark.unit
class TestJwtService:
    @pytest.mark.django_db
    def test_generate_access_refresh_token(self, test_user, test_iss_service):
        access_token, refresh_token = test_iss_service.generate_access_and_refresh_tokens(tg_user=test_user)
        access_payload = test_iss_service.decode_token(access_token)
        refresh_payload = test_iss_service.decode_token(refresh_token)
        assert access_payload["tguser_id"] == str(test_user.telegram_id)
        assert access_payload["type"] == "access"

        assert refresh_payload["tguser_id"] == str(test_user.telegram_id)
        assert refresh_payload["type"] == "refresh"
