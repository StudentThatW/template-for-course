import pytest


@pytest.mark.unit
class TestBankService:
    @pytest.mark.django_db
    def test_get_bank_account_balance(self, test_user_with_account_and_card, test_bank_service):
        amount = test_bank_service.get_bank_account_by_account_id(test_user_with_account_and_card[1].id).amount
        assert amount == 1000

    @pytest.mark.django_db
    def test_get_card_balance(self, test_user_with_account_and_card, test_bank_service):
        amount = test_bank_service.get_card_by_number(test_user_with_account_and_card[2].number).bank_account.amount
        assert amount == 1000

    @pytest.mark.django_db
    def test_get_bank_account_by_telegram_id(self, test_user_with_account_and_card, test_bank_service):
        tguser = test_user_with_account_and_card[0]
        bank_account = test_bank_service.get_bank_account_by_telegram_id(tguser.telegram_id)
        assert test_user_with_account_and_card[1] == bank_account

    @pytest.mark.django_db
    def test_get_bank_account_by_tguser_name(self, test_user_with_account_and_card, test_bank_service):
        tguser = test_user_with_account_and_card[0]
        bank_account = test_bank_service.get_bank_account_by_tguser_name(tguser.username)
        assert test_user_with_account_and_card[1] == bank_account

    @pytest.mark.django_db
    def test_transfer_money(self, test_user_with_account_and_card, test_another_user_with_account_and_card, test_bank_service):
        bank_account1 = test_user_with_account_and_card[1]
        bank_account2 = test_another_user_with_account_and_card[1]
        test_bank_service.transfer_money(bank_account1, bank_account2, 500)
        new_amount_sender = test_bank_service.get_bank_account_by_account_id(test_user_with_account_and_card[1].id).amount
        new_amount_receiver = test_bank_service.get_bank_account_by_account_id(test_another_user_with_account_and_card[1].id).amount
        assert new_amount_sender == 500
        assert new_amount_receiver == 2500
