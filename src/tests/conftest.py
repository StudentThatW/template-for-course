import pytest

from app.internal.authentification.db.repositories import IssuedTokenRepository
from app.internal.authentification.domain.services import IssuedTokenService
from app.internal.authentification.presentation.handlers import IssuedTokenHandlers
from app.internal.bank_account_and_card.db.models import BankAccount, Card
from app.internal.bank_account_and_card.db.repositories import BankAccountCardRepository
from app.internal.bank_account_and_card.domain.services import BankAccountCardService
from app.internal.bank_account_and_card.presentation.bot_handlers import BankAccountCardBotHandlers
from app.internal.favourite_users.db.repositories import FavouriteTgUsersRepository
from app.internal.favourite_users.domain.services import FavouriteTgUsersService
from app.internal.favourite_users.presentation.bot_handlers import FavouriteTgUsersBotHandlers
from app.internal.tg_users.db.models import TgUser
from app.internal.tg_users.db.repositories import TgUserRepository
from app.internal.tg_users.domain.services import TgUserService
from app.internal.tg_users.presentation.bot_handlers import TgUsersBotHandlers


@pytest.fixture(scope="function")
def test_user(telegram_id=5, first_name="first_name1", username="username1"):
    return TgUser.objects.create(
        telegram_id=telegram_id, first_name=first_name, username=username, phone_number="77777777777"
    )


@pytest.fixture(scope="function")
def test_user_without_phone(telegram_id=6, first_name="first_name2", username="username2"):
    return TgUser.objects.create(
        telegram_id=telegram_id,
        first_name=first_name,
        username=username,
    )


@pytest.fixture(scope="function")
def test_user_with_favourite_user(telegram_id1=7, first_name1="first_name3", telegram_id2=8, first_name2="first_name4"):
    first_user = TgUser.objects.create(telegram_id=telegram_id1, first_name=first_name1, username=first_name1)
    second_user = TgUser.objects.create(telegram_id=telegram_id2, first_name=first_name2, username=first_name2)
    return first_user, second_user


@pytest.fixture(scope="function")
def test_user_with_account_and_card():
    user1 = TgUser.objects.create(
        telegram_id=9, first_name="first_name5", username="bank_user1", phone_number="79999999999"
    )
    bank_account1 = BankAccount.objects.create(id=3, tg_user=user1, amount=1000)
    card1 = Card.objects.create(number=3, bank_account=bank_account1)
    return user1, bank_account1, card1


@pytest.fixture(scope="function")
def test_another_user_with_account_and_card():
    user2 = TgUser.objects.create(
        telegram_id=10, first_name="first_name6", username="bank_user2", phone_number="79999999998"
    )
    bank_account2 = BankAccount.objects.create(id=4, tg_user=user2, amount=2000)
    card2 = Card.objects.create(number=4, bank_account=bank_account2)
    return user2, bank_account2, card2


@pytest.fixture(scope="function")
def test_tguser_service():
    tguser_repo = TgUserRepository()
    return TgUserService(tguser_repo)


@pytest.fixture(scope="function")
def test_tguser_bot_handler(test_tguser_service):
    return TgUsersBotHandlers(test_tguser_service)


@pytest.fixture(scope="function")
def test_iss_service():
    iss_repo = IssuedTokenRepository()
    return IssuedTokenService(iss_repo)


@pytest.fixture(scope="function")
def test_iss_handler(test_iss_service):
    return IssuedTokenHandlers(test_iss_service)


@pytest.fixture(scope="function")
def test_favourite_tguser_service():
    fav_repo = FavouriteTgUsersRepository()
    return FavouriteTgUsersService(fav_repo)


@pytest.fixture(scope="function")
def test_favourite_tguser_bot_handler(test_favourite_tguser_service, test_tguser_service):
    return FavouriteTgUsersBotHandlers(test_favourite_tguser_service, test_tguser_service)


@pytest.fixture(scope="function")
def test_bank_service():
    bank_repo = BankAccountCardRepository()
    return BankAccountCardService(bank_repo)


@pytest.fixture(scope="function")
def test_bank_handler(test_bank_service, test_favourite_tguser_service, test_tguser_service):
    return BankAccountCardBotHandlers(test_bank_service, test_favourite_tguser_service, test_tguser_service)


@pytest.fixture(scope="function")
def test_user_with_password(test_tguser_service):
    user2 = TgUser.objects.create(
        telegram_id=14, first_name="first_name6", username="bank_user2", phone_number="79999999998"
    )
    test_tguser_service.set_user_password(user2, "dez")
    return user2
