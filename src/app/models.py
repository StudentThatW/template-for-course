from django.contrib.auth.models import AbstractUser

from app.internal.authentification.db.models import IssuedToken
from app.internal.bank_account_and_card.db.models import BankAccount, Card, MoneyTransaction
from app.internal.favourite_users.db.models import FavouriteTgUser
from app.internal.tg_users.db.models import TgUser


class AdminUser(AbstractUser):
    pass
