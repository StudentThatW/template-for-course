from ninja import NinjaAPI

from app.internal.tg_users.db.repositories import TgUserRepository
from app.internal.tg_users.domain.services import TgUserService
from app.internal.tg_users.presentation.handlers import TgUsersHandlers
from app.internal.tg_users.presentation.routers import add_tg_users_router


def add_tg_user_api(api: NinjaAPI):
    tguser_repo = TgUserRepository()
    tguser_service = TgUserService(tguser_repo)
    tguser_handler = TgUsersHandlers(tguser_service)
    add_tg_users_router(api, tguser_handler)
