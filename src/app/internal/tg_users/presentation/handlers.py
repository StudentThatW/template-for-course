from typing import List

from django.db.utils import IntegrityError
from ninja import Body

from app.internal.responses.error_response import ErrorResponse
from app.internal.responses.success_response import SuccessResponse
from app.internal.tg_users.domain.entities import TelephoneSchema
from app.internal.tg_users.domain.services import TgUserService


class TgUsersHandlers:
    def __init__(self, tguser_service: TgUserService):
        self._tguser_service = tguser_service

    def get_tg_user_info(self, request, tg_id: int):
        user, message = self._tguser_service.check_and_get_user(tg_id)
        if user is None:
            return 400, ErrorResponse(message=message)

        return 200, user

    def put_tg_user_info(self, request, tg_id: int, phone_data: TelephoneSchema = Body(...)):
        user, message = self._tguser_service.check_and_get_user(tg_id)
        if user is None:
            return 400, ErrorResponse(message=message)
        phone = phone_data.phone_number
        self._tguser_service.set_user_phone(user.telegram_id, phone)

        return 200, {"success": True}
