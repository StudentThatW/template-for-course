from django.contrib import admin

from app.internal.tg_users.db.models import TgUser


@admin.register(TgUser)
class TgUserAdmin(admin.ModelAdmin):
    pass
