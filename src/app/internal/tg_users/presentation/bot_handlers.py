import json
import re

from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.decorators.check_number import phone_required
from app.internal.responses.responses_strings import (
    GET_INTERACTIONS_START_REPLY,
    GET_TRANSACTIONS_START_REPLY,
    INVALID_ARGUMENTS_SET_PHONE_REPLY,
    PHONE_NUMBER_NOT_SAVED_REPLY,
    PHONE_NUMBER_SAVED_REPLY,
    START_HELLO_REPLY,
)
from app.internal.tg_users.domain.services import TgUserService


class TgUsersBotHandlers:
    def __init__(self, tguser_service: TgUserService):
        self._tguser_service = tguser_service

    def start(self, update: Update, context: CallbackContext) -> None:
        user = update.effective_user
        self._tguser_service.create_user(
            first_name=user.first_name, telegram_id=user.id, username=user.username,
        )
        update.message.reply_text(START_HELLO_REPLY.format(first_name=update.effective_user.first_name))

    def set_phone(self, update: Update, context: CallbackContext) -> None:
        if len(context.args) != 1:
            update.message.reply_text(INVALID_ARGUMENTS_SET_PHONE_REPLY)
        elif not re.match(r"^\+?1?\d{9,15}$", context.args[0]):
            update.message.reply_text(INVALID_ARGUMENTS_SET_PHONE_REPLY)
            return
        else:
            self._tguser_service.set_user_phone(update.effective_user.id, context.args[0])
            update.message.reply_text(PHONE_NUMBER_SAVED_REPLY)

    @phone_required
    def me(self, update: Update, context: CallbackContext) -> None:
        user = self._tguser_service.get_user(update.effective_user.id)
        update.message.reply_text(json.dumps(self._tguser_service.serialize_user(user), indent=2, ensure_ascii=False))

    @phone_required
    def set_password(self, update: Update, context: CallbackContext):
        if len(context.args) != 1:
            update.message.reply_text("Need password")
            return
        user = self._tguser_service.get_user(update.effective_user.id)
        password = str(context.args[0])
        self._tguser_service.set_user_password(user, password)
        update.message.reply_text("Password saved")
