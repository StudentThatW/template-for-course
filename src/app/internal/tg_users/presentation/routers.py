from typing import List

from ninja import NinjaAPI, Router
from ninja.errors import HttpError

from app.internal.responses.error_response import ErrorResponse
from app.internal.responses.success_response import SuccessResponse
from app.internal.tg_users.domain.entities import TgUserOut
from app.internal.tg_users.presentation.handlers import TgUsersHandlers


def add_tg_users_router(api: NinjaAPI, fav_user_handlers: TgUsersHandlers):
    users_router = get_users_router(fav_user_handlers)
    api.add_router("/tg_users", users_router)


def get_users_router(fav_user_handlers: TgUsersHandlers):
    router = Router(tags=["tg_users"])

    router.add_api_operation("{tg_id}/me", ["GET"], fav_user_handlers.get_tg_user_info, response={200: TgUserOut, 400: ErrorResponse},)
    router.add_api_operation("{tg_id}/phone", ["PUT"], fav_user_handlers.put_tg_user_info, response={200: SuccessResponse, 400: ErrorResponse})
    return router
