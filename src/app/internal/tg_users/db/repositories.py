import hashlib
from abc import ABC, abstractmethod
from typing import List, Optional

from django.db.models import Q

from app.internal.favourite_users.db.models import FavouriteTgUser
from app.internal.tg_users.db.models import TgUser
from app.internal.tg_users.domain.entities import TgUserIn, TgUserOut
from config.settings import PASSWORD_SALT


class ITgUserRepository(ABC):
    @abstractmethod
    def create_user(self, telegram_id: int, first_name: str, username: str) -> None:
        ...

    @abstractmethod
    def set_user_phone(self, telegram_id: int, phone: str) -> None:
        ...

    @abstractmethod
    def serialize_user(self, user: TgUserIn) -> dict:
        ...

    @abstractmethod
    def get_user(self, telegram_id: int) -> TgUserOut:
        ...

    @abstractmethod
    def is_user_has_no_phone(self, user: TgUserIn) -> bool:
        ...

    @abstractmethod
    def is_user_exist(self, telegram_id: int) -> bool:
        ...

    @abstractmethod
    def set_user_password(self, user: TgUserIn, password: str) -> None:
        ...

    @abstractmethod
    def get_hash(self, password: str) -> bytes:
        ...

    @abstractmethod
    def check_and_get_user(self, telegram_id: str) -> (TgUserOut, Optional[str]):
        ...


class TgUserRepository(ITgUserRepository):
    def create_user(self, telegram_id: int, first_name: str, username: str) -> None:
        TgUser.objects.get_or_create(
            telegram_id=telegram_id, defaults={"username": username, "first_name": first_name, },
        )

    def set_user_phone(self, telegram_id: int, phone: str) -> None:
        user_obj = TgUser.objects.get_or_create(telegram_id=telegram_id)[0]
        user_obj.phone_number = phone
        user_obj.save()

    def serialize_user(self, user: TgUserIn) -> dict:
        user_info = {
            "username": user.username,
            "first_name": user.first_name,
            "telegram_id": user.telegram_id,
            "phone number": str(user.phone_number),
        }
        return user_info

    def get_user(self, telegram_id: int) -> TgUserOut:
        return TgUser.objects.get_or_create(telegram_id=telegram_id)[0]

    def is_user_has_no_phone(self, user: TgUserIn) -> bool:
        return user is None or not user.phone_number

    def is_user_exist(self, telegram_id: int) -> bool:
        return TgUser.objects.filter(Q(telegram_id=telegram_id)).first() is not None

    def set_user_password(self, user: TgUserIn, password: str) -> None:
        hashed_password = self.get_hash(password)
        user.password_hash = hashed_password
        user.save(update_fields=["password_hash"])

    def get_hash(self, password: str) -> bytes:
        return hashlib.pbkdf2_hmac(
            "sha256", password.encode("utf-8"), PASSWORD_SALT.encode("utf-8"), 100000
        )

    def check_and_get_user(self, telegram_id: str) -> (TgUserOut, Optional[str]):
        if not self.is_user_exist(telegram_id):
            return None, f"No user with id {telegram_id}"
        return self.get_user(telegram_id), None
