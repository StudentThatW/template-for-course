from django.db import models


class TgUser(models.Model):
    first_name = models.CharField(max_length=255)
    telegram_id = models.IntegerField(unique=True, primary_key=True)
    username = models.CharField(max_length=255, null=True, unique=True)
    phone_number = models.CharField(max_length=17, blank=True)
    password_hash = models.BinaryField(editable=True, null=True, max_length=65000)

    def __str__(self):
        return f"TgUser: {self.username}"

    class Meta:
        db_table = "tg_user"
