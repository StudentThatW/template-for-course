from typing import List, Optional

from app.internal.tg_users.db.repositories import ITgUserRepository
from app.internal.tg_users.domain.entities import TgUserIn, TgUserOut


class TgUserService:
    def __init__(self, tguser_repo: ITgUserRepository):
        self._tguser_repo = tguser_repo

    def create_user(self, telegram_id: int, first_name: str, username: str) -> None:
        self._tguser_repo.create_user(telegram_id, first_name, username)

    def set_user_phone(self, telegram_id: int, phone: str) -> None:
        self._tguser_repo.set_user_phone(telegram_id, phone)

    def serialize_user(self, user: TgUserIn) -> dict:
        return self._tguser_repo.serialize_user(user)

    def get_user(self, telegram_id: int) -> TgUserOut:
        return self._tguser_repo.get_user(telegram_id)

    def is_user_has_no_phone(self, user: TgUserIn) -> bool:
        return self._tguser_repo.is_user_has_no_phone(user)

    def is_user_exist(self, telegram_id: int) -> bool:
        return self._tguser_repo.is_user_exist(telegram_id)

    def set_user_password(self, user: TgUserIn, password: str) -> None:
        self._tguser_repo.set_user_password(user, password)

    def get_hash(self, password: str) -> bytes:
        return self._tguser_repo.get_hash(password)

    def check_and_get_user(self, telegram_id: str) -> (TgUserOut, Optional[str]):
        return self._tguser_repo.check_and_get_user(telegram_id)
