from ninja import Schema
from ninja.orm import create_schema

from app.internal.tg_users.db.models import TgUser

TgUserSchema = create_schema(TgUser)


class TgUserOut(TgUserSchema):
    ...


class TgUserIn(TgUserSchema):
    ...


class TelephoneSchema(Schema):
    phone_number: str
