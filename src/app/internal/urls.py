from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from ninja import NinjaAPI

from app.internal.authentification.app import add_token_api
from app.internal.favourite_users.app import add_fav_user_api
from app.internal.tg_users.app import add_tg_user_api


def get_api() -> NinjaAPI:
    api = NinjaAPI(
        title="backend-course",
        version="1.0.0",
    )

    add_fav_user_api(api)
    add_token_api(api)
    add_tg_user_api(api)

    return api


apii = get_api()


urlpatterns = [
    path("special_api/", apii .urls),
]
