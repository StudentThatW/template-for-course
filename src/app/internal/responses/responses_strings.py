START_HELLO_REPLY = "Hello, {first_name}"

INVALID_ARGUMENTS_SET_PHONE_REPLY = "Command must entered in the format: /set_phone +999999999, 9 TO 15 digits allowed"
PHONE_NUMBER_SAVED_REPLY = "Phone number saved"

PHONE_NUMBER_NOT_SAVED_REPLY = "Save phone number first"

INVALID_ARGUMENTS_GET_ACCOUNT_AMOUNT_REPLY = "Command must entered in the format: /bank_account {bank account id}"
NO_RIGHTS_FOR_BANK_REPLY = "There is no bank account or you have no rights on it"

INVALID_ARGUMENTS_CARD_AMOUNT_REPLY = "Command must entered in the format: /card {card number}"

INVALID_ARGUMENTS_TRANSFER_AMOUNT_REPLY = "Command must entered in the format: /transfer_amount {your_account_id} account/card/username {account_id/card number/username} {sum}"

NO_SUCH_ACCOUNT_REPLY = "No such bank account"
USER_NOT_FAVOURITE_REPLY = "User is not your favourite"
MONEY_TRANSFERED_REPLY = "Money transfered, your current balance on account {account_id} is {amount}"

FAVOURITE_USERS_START_REPLY = "Favourite users:\n\n"
USER_ADDED_TO_FAVOURITES = "User added to favourites"
USER_DELETED_FROM_FAVOURITES = "User deleted from favourites"

INVALID_ARGUMENTS_GET_TRANSACTIONS_REPLY = "Command must entered in the format: /get_transactions {bank account id}"
GET_INTERACTIONS_START_REPLY = "Interacted with\n"
GET_TRANSACTIONS_START_REPLY = "Transactions for {account_id}\n\n"
