from typing import List

from django.db.utils import IntegrityError
from ninja import Body
from ninja.errors import HttpError

from app.internal.favourite_users.domain.entities import PossessorFavouriteSchema
from app.internal.favourite_users.domain.services import FavouriteTgUsersService
from app.internal.responses.error_response import ErrorResponse
from app.internal.responses.success_response import SuccessResponse
from app.internal.tg_users.domain.services import TgUserService


class FavouriteTgUsersHandlers:
    def __init__(self, favourite_user_service: FavouriteTgUsersService, tguser_service: TgUserService):
        self._favourite_user_service = favourite_user_service
        self._tguser_service = tguser_service

    def get_fav_user_info(self, request, tg_id: int):
        user, message = self._tguser_service.check_and_get_user(tg_id)
        if user is None:
            return 400, ErrorResponse(message=message)
        result = self._favourite_user_service.get_favourite_tgusers(user)
        return result

    def rem_fav_user_info(self, request, poss_fav_data: PossessorFavouriteSchema = Body(...)):
        possessor, pos_message = self._tguser_service.check_and_get_user(poss_fav_data.possessor_id)
        if possessor is None:
            return 400, ErrorResponse(message=pos_message)

        favourite, fav_message = self._tguser_service.check_and_get_user(poss_fav_data.favourite_id)
        if favourite is None:
            return 400, ErrorResponse(message=fav_message)

        result = self._favourite_user_service.remove_favorite_tguser(possessor, favourite)
        if result is not None:
            return 400, ErrorResponse(message=result)
        return {"success": True}
