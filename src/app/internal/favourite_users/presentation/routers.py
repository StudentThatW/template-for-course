from typing import List

from ninja import Body, NinjaAPI, Router
from ninja.errors import HttpError

from app.internal.favourite_users.domain.entities import FavouriteTgUserOut
from app.internal.favourite_users.presentation.handlers import FavouriteTgUsersHandlers
from app.internal.responses.error_response import ErrorResponse
from app.internal.responses.success_response import SuccessResponse


def add_users_router(api: NinjaAPI, fav_user_handlers: FavouriteTgUsersHandlers):
    users_router = get_users_router(fav_user_handlers)
    api.add_router("/favourite_users", users_router)


def get_users_router(fav_user_handlers: FavouriteTgUsersHandlers):
    router = Router(tags=["favourite_users"])

    router.add_api_operation("{tg_id}/list", ["GET"], fav_user_handlers.get_fav_user_info, response={200: List[FavouriteTgUserOut], 400: ErrorResponse},)

    router.add_api_operation("delete", ["DELETE"], fav_user_handlers.rem_fav_user_info, response={200: SuccessResponse, 400: ErrorResponse})
    return router
