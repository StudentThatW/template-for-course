from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.decorators.check_number import phone_required
from app.internal.favourite_users.domain.services import FavouriteTgUsersService
from app.internal.responses.responses_strings import (
    FAVOURITE_USERS_START_REPLY,
    USER_ADDED_TO_FAVOURITES,
    USER_DELETED_FROM_FAVOURITES,
)
from app.internal.tg_users.domain.services import TgUserService


class FavouriteTgUsersBotHandlers:
    def __init__(self, favourite_user_service: FavouriteTgUsersService, _tguser_service: TgUserService):
        self._favourite_user_service = favourite_user_service
        self.tguser_service = _tguser_service

    @phone_required
    def get_favorite(self, update: Update, context: CallbackContext):
        user, message = self.tguser_service.check_and_get_user(update.effective_user.id)
        if user is None:
            update.message.reply_text(message)
            return

        favorite_tgusers = self._favourite_user_service.get_favourite_tgusers(user)
        message = FAVOURITE_USERS_START_REPLY
        for index, favorite_tguser in enumerate(favorite_tgusers):
            message += f"{index + 1}. {favorite_tguser.favorite.telegram_id}\n"
        update.message.reply_text(message)

    @phone_required
    def add_favorite(self, update: Update, context: CallbackContext):
        user, message = self.tguser_service.check_and_get_user(update.effective_user.id)
        if user is None:
            update.message.reply_text(message)
            return
        favourite, fav_message = self.tguser_service.check_and_get_user(str(context.args[0]))
        if favourite is None:
            update.message.reply_text(fav_message)
            return

        self._favourite_user_service.add_favorite_tguser(user, favourite)
        update.message.reply_text(USER_ADDED_TO_FAVOURITES)

    @phone_required
    def remove_favorite(self, update: Update, context: CallbackContext):
        user, message = self.tguser_service.check_and_get_user(update.effective_user.id)
        if user is None:
            update.message.reply_text(message)
            return
        favourite, fav_message = self.tguser_service.check_and_get_user(str(context.args[0]))
        if favourite is None:
            update.message.reply_text(fav_message)
            return

        result = self._favourite_user_service.remove_favorite_tguser(user, favourite)
        if result is not None:
            update.message.reply_text(result)
            return
        update.message.reply_text(USER_DELETED_FROM_FAVOURITES)
