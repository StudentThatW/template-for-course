from django.contrib import admin

from app.internal.favourite_users.db.models import FavouriteTgUser


@admin.register(FavouriteTgUser)
class FavouriteTgUserAdmin(admin.ModelAdmin):
    list_display = ("possessor", "favorite")
    pass
