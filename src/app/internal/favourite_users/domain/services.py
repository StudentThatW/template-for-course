from typing import List, Optional

from app.internal.favourite_users.db.models import FavouriteTgUser
from app.internal.favourite_users.db.repositories import IFavouriteTgUsersRepository
from app.internal.favourite_users.domain.entities import FavouriteTgUserOut
from app.internal.tg_users.domain.entities import TgUserIn


class FavouriteTgUsersService:
    def __init__(self, favourite_tguser_repo: IFavouriteTgUsersRepository):
        self._favourite_tguser_repo = favourite_tguser_repo

    def get_favourite_tgusers(self, possessor: TgUserIn) -> List[FavouriteTgUserOut]:
        return self._favourite_tguser_repo.get_favourite_tgusers(possessor)

    def add_favorite_tguser(self, possessor: TgUserIn, favorite: TgUserIn) -> None:
        self._favourite_tguser_repo.add_favorite_tguser(possessor, favorite)

    def check_is_favourite_tguser(self, possessor: TgUserIn, favorite: TgUserIn) -> bool:
        return self._favourite_tguser_repo.check_is_favourite_tguser(possessor, favorite)

    def remove_favorite_tguser(self, possessor: TgUserIn, favorite: TgUserIn):
        result = self._favourite_tguser_repo.remove_favorite_tguser(possessor, favorite)
        if result[0] != 1:
            return "User was not favourite"
        return None
