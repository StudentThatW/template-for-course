from ninja import Schema
from ninja.orm import create_schema

from app.internal.favourite_users.db.models import FavouriteTgUser

FavouriteTgUserSchema = create_schema(FavouriteTgUser)


class FavouriteTgUserOut(FavouriteTgUserSchema):
    ...


class PossessorFavouriteSchema(Schema):
    possessor_id: str
    favourite_id: str
