from abc import ABC, abstractmethod
from typing import List

from app.internal.favourite_users.db.models import FavouriteTgUser
from app.internal.tg_users.domain.entities import TgUserIn


class IFavouriteTgUsersRepository(ABC):
    @abstractmethod
    def get_favourite_tgusers(self, possessor: TgUserIn) -> List[FavouriteTgUser]:
        ...

    @abstractmethod
    def add_favorite_tguser(self, possessor: TgUserIn, favorite: TgUserIn) -> None:
        ...

    @abstractmethod
    def check_is_favourite_tguser(self, user: TgUserIn) -> bool:
        ...

    @abstractmethod
    def remove_favorite_tguser(self, possessor: TgUserIn, favorite: TgUserIn) -> int:
        ...


class FavouriteTgUsersRepository(IFavouriteTgUsersRepository):
    def get_favourite_tgusers(self, possessor: TgUserIn) -> List[FavouriteTgUser]:
        return list(FavouriteTgUser.objects.select_related().filter(possessor=possessor))

    def add_favorite_tguser(self, possessor: TgUserIn, favorite: TgUserIn) -> None:
        FavouriteTgUser.objects.update_or_create(possessor=possessor, favorite=favorite)

    def check_is_favourite_tguser(self, possessor: TgUserIn, favorite: TgUserIn) -> bool:
        return FavouriteTgUser.objects.filter(possessor=possessor, favorite=favorite).exists()

    def remove_favorite_tguser(self, possessor: TgUserIn, favorite: TgUserIn) -> int:
        return FavouriteTgUser.objects.filter(possessor=possessor, favorite=favorite).delete()
