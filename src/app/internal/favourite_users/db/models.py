from django.db import models

from app.internal.tg_users.db.models import TgUser


class FavouriteTgUser(models.Model):
    possessor = models.ForeignKey(TgUser, on_delete=models.CASCADE, related_name="favorite_possessor")
    favorite = models.ForeignKey(TgUser, on_delete=models.CASCADE, related_name="favorite_itself")

    def __str__(self):
        return f"FavouriteTgUser {self.favorite.telegram_id} for {self.possessor.telegram_id}"
