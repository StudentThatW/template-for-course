from ninja import NinjaAPI

from app.internal.favourite_users.db.repositories import FavouriteTgUsersRepository
from app.internal.favourite_users.domain.services import FavouriteTgUsersService
from app.internal.favourite_users.presentation.handlers import FavouriteTgUsersHandlers
from app.internal.favourite_users.presentation.routers import add_users_router
from app.internal.tg_users.db.repositories import TgUserRepository
from app.internal.tg_users.domain.services import TgUserService


def add_fav_user_api(api: NinjaAPI):
    fav_user_repo = FavouriteTgUsersRepository()
    fav_user_service = FavouriteTgUsersService(fav_user_repo)

    tguser_repo = TgUserRepository()
    tguser_service = TgUserService(tguser_repo)

    fav_user_handler = FavouriteTgUsersHandlers(fav_user_service, tguser_service)
    add_users_router(api, fav_user_handler)
