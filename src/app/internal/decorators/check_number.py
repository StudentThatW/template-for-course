from functools import wraps

from telegram import Update
from telegram.ext import CallbackContext

from app.internal.responses.responses_strings import PHONE_NUMBER_NOT_SAVED_REPLY
from app.internal.tg_users.db.repositories import TgUserRepository
from app.internal.tg_users.domain.services import TgUserService


def phone_required(telegram_command_handler):
    @wraps(telegram_command_handler)
    def wrapper(self, update: Update, context: CallbackContext):
        tguser_repo = TgUserRepository()
        tguser_service = TgUserService(tguser_repo)
        user = tguser_service.get_user(update.effective_user.id)
        if tguser_service.is_user_has_no_phone(user):
            update.message.reply_text(PHONE_NUMBER_NOT_SAVED_REPLY)
        else:
            telegram_command_handler(self, update, context)

    return wrapper
