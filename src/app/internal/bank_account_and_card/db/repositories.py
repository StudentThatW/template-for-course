from abc import ABC, abstractmethod
from itertools import chain
from typing import List

from django.db import IntegrityError, transaction
from django.db.models import F, Q, Sum

from app.internal.bank_account_and_card.db.models import BankAccount, Card, MoneyTransaction
from app.internal.tg_users.domain.entities import TgUserIn, TgUserOut


class IBankAccountCardRepository(ABC):
    @abstractmethod
    def get_bank_account_by_account_id(self, account_id: int) -> BankAccount | None:
        ...

    @abstractmethod
    def check_is_bank_account_belongs_to_user(self, account_id: int, user: TgUserIn) -> bool:
        ...

    @abstractmethod
    def get_card_by_number(self, card_name: int) -> Card | None:
        ...

    @abstractmethod
    def get_bank_account_by_telegram_id(self, telegram_id: int) -> BankAccount | None:
        ...

    @abstractmethod
    def get_bank_account_by_card_id(self, card_number: int) -> BankAccount | None:
        ...

    @abstractmethod
    def get_bank_account_by_tguser_name(self, tguser_name: str) -> BankAccount | None:
        ...

    @abstractmethod
    def transfer_money(self, sender_account: BankAccount, receiver_account: BankAccount, money_amount: int) -> bool:
        ...

    @abstractmethod
    def get_usernames_from_interactions(self, tg_id: str) -> List[str]:
        ...

    @abstractmethod
    def get_money_transactions_for_account(self, tg_id: str, account_id: str) -> List[MoneyTransaction]:
        ...


class BankAccountCardRepository(IBankAccountCardRepository):
    def get_bank_account_by_account_id(self, account_id: int) -> BankAccount | None:
        return BankAccount.objects.select_related().filter(id=account_id).first()

    def check_is_bank_account_belongs_to_user(self, account_id: int, user: TgUserIn) -> bool:
        return BankAccount.objects.filter(id=account_id, tg_user=user).exists()

    def get_card_by_number(self, card_name: int) -> Card | None:
        return Card.objects.select_related().filter(number=card_name).first()

    def transfer_money(self,
                       sender_account: BankAccount, receiver_account: BankAccount, money_amount: int
                       ) -> bool:
        sender_account.amount = F("amount") - money_amount
        receiver_account.amount = F("amount") + money_amount
        try:
            with transaction.atomic():
                sender_account.save(update_fields=("amount",))
                receiver_account.save(update_fields=("amount",))
                MoneyTransaction.objects.create(sender=sender_account, receiver=receiver_account, amount=money_amount)
        except IntegrityError:
            return False
        return True

    def get_bank_account_by_telegram_id(self, telegram_id: int) -> BankAccount | None:
        return BankAccount.objects.select_related().filter(tg_user__telegram_id=telegram_id).first()

    def get_bank_account_by_card_id(self, card_number: int) -> BankAccount | None:
        try:
            card = Card.objects.get(number=card_number)
        except Card.DoesNotExist:
            return None
        bank_account = card.bank_account
        return bank_account

    def get_bank_account_by_tguser_name(self, tguser_name: str) -> BankAccount | None:
        return BankAccount.objects.select_related().filter(tg_user__username=tguser_name).first()

    def get_bank_account_user(self, recipient: BankAccount) -> TgUserOut | None:
        return recipient.tg_user

    def get_usernames_from_interactions(self, tg_id: str) -> List[str]:
        usernames_to = MoneyTransaction.objects.filter(sender__tg_user__telegram_id=tg_id).values_list(
            "receiver__tg_user__username", flat=True
        )
        usernames_from = MoneyTransaction.objects.filter(receiver__tg_user__telegram_id=tg_id).values_list(
            "sender__tg_user__username", flat=True
        )
        targets = usernames_from.union(usernames_to).all()
        return list(targets)

    def get_money_transactions_for_account(self, tg_id: str, account_id: str) -> List[MoneyTransaction]:
        return (
            MoneyTransaction.objects.select_related()
            .filter(
                Q(sender__tg_user__telegram_id=tg_id, sender__id=account_id)
                | Q(receiver__tg_user__telegram_id=tg_id, receiver__id=account_id)
            )
            .distinct()
        )
