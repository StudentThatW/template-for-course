from django.db import models

from app.internal.tg_users.db.models import TgUser


class BankAccount(models.Model):
    tg_user = models.ForeignKey(TgUser, on_delete=models.CASCADE, default=0)
    id = models.AutoField(auto_created=True, primary_key=True)
    amount = models.PositiveBigIntegerField()

    def __str__(self):
        return f"Bank Account with id: {self.id}"


class Card(models.Model):
    number = models.AutoField(auto_created=True, primary_key=True)
    bank_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE)

    def __str__(self):
        return f"Card with number: {self.number}"


class MoneyTransaction(models.Model):
    sender = models.ForeignKey(BankAccount, on_delete=models.SET_NULL, null=True, related_name="transaction_from")
    receiver = models.ForeignKey(BankAccount, on_delete=models.SET_NULL, null=True, related_name="transaction_to")
    amount = models.PositiveBigIntegerField()
    date = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return f"Transaction from ${self.date} on amount ${self.amount}"
