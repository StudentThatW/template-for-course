from ninja import Schema
from ninja.orm import create_schema

from app.internal.bank_account_and_card.db.models import BankAccount, Card, MoneyTransaction

BankAccountSchema = create_schema(BankAccount)


class BankAccountOut(BankAccountSchema):
    ...


class BankAccountIn(BankAccountSchema):
    ...


CardSchema = create_schema(Card)


class CardOut(CardSchema):
    ...


MoneyTransactionSchema = create_schema(MoneyTransaction)


class MoneyTransactionOut(MoneyTransactionSchema):
    ...


class MoneyTransactionIn(MoneyTransactionSchema):
    ...
