from typing import List, Optional

from app.internal.bank_account_and_card.db.repositories import IBankAccountCardRepository
from app.internal.bank_account_and_card.domain.entities import (
    BankAccountIn,
    BankAccountOut,
    CardOut,
    MoneyTransactionOut,
)
from app.internal.tg_users.domain.entities import TgUserIn


class BankAccountCardService:
    def __init__(self, bank_repo: IBankAccountCardRepository):
        self._bank_repo = bank_repo

    def get_bank_account_by_account_id(self, account_id: int) -> BankAccountOut | None:
        return self._bank_repo.get_bank_account_by_account_id(account_id)

    def check_is_bank_account_belongs_to_user(self, account_id: int, user: TgUserIn) -> bool:
        return self._bank_repo.check_is_bank_account_belongs_to_user(account_id, user)

    def get_card_by_number(self, card_name: int) -> CardOut | None:
        return self._bank_repo.get_card_by_number(card_name)

    def transfer_money(self,
                       sender_account: BankAccountIn, receiver_account: BankAccountIn, money_amount: int
                       ) -> bool:
        return self._bank_repo.transfer_money(sender_account, receiver_account, money_amount)

    def get_bank_account_by_telegram_id(self, telegram_id: int) -> BankAccountOut | None:
        return self._bank_repo.get_bank_account_by_telegram_id(telegram_id)

    def get_bank_account_by_card_id(self, card_number: int) -> BankAccountOut | None:
        return self._bank_repo.get_bank_account_by_card_id(card_number)

    def get_bank_account_by_tguser_name(self, tguser_name: str) -> BankAccountOut | None:
        return self._bank_repo.get_bank_account_by_tguser_name(tguser_name)

    def get_usernames_from_interactions(self, tg_id: str) -> List[str]:
        return self._bank_repo.get_usernames_from_interactions(tg_id)

    def get_money_transactions_for_account(self, tg_id: str, account_id: str) -> List[MoneyTransactionOut]:
        return self._bank_repo.get_money_transactions_for_account(tg_id, account_id)
