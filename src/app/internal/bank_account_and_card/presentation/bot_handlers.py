import re
from typing import List

from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.bank_account_and_card.domain.entities import MoneyTransactionIn
from app.internal.bank_account_and_card.domain.services import BankAccountCardService
from app.internal.decorators.check_number import phone_required
from app.internal.favourite_users.domain.services import FavouriteTgUsersService
from app.internal.responses.responses_strings import (
    GET_INTERACTIONS_START_REPLY,
    GET_TRANSACTIONS_START_REPLY,
    INVALID_ARGUMENTS_CARD_AMOUNT_REPLY,
    INVALID_ARGUMENTS_GET_ACCOUNT_AMOUNT_REPLY,
    INVALID_ARGUMENTS_GET_TRANSACTIONS_REPLY,
    INVALID_ARGUMENTS_SET_PHONE_REPLY,
    INVALID_ARGUMENTS_TRANSFER_AMOUNT_REPLY,
    MONEY_TRANSFERED_REPLY,
    NO_RIGHTS_FOR_BANK_REPLY,
    NO_SUCH_ACCOUNT_REPLY,
    USER_NOT_FAVOURITE_REPLY,
)
from app.internal.tg_users.domain.services import TgUserService


class BankAccountCardBotHandlers:
    def __init__(self, bank_service: BankAccountCardService, fav_service: FavouriteTgUsersService, _tguser_service: TgUserService):
        self._bank_service = bank_service
        self._fav_service = fav_service
        self.tguser_service = _tguser_service
        self.commands_dictionary = {
            "account": lambda account_id: self._bank_service.get_bank_account_by_account_id(account_id),
            "card": lambda card_id: self._bank_service.get_bank_account_by_card_id(card_id),
            "username": lambda username: self._bank_service.get_bank_account_by_tguser_name(username),
        }

    def check_validity(self, input_string: str) -> bool:
        return re.match(r"\d+ (?:account|card) \d+ \d+", input_string) or re.match(r"\d+ username [a-zA-Z0-9_]+ \d+", input_string)

    @phone_required
    def get_account_amount(self, update: Update, context: CallbackContext) -> None:
        if len(context.args) != 1:
            update.message.reply_text(INVALID_ARGUMENTS_GET_ACCOUNT_AMOUNT_REPLY)
        else:
            user = self.tguser_service.get_user(update.effective_user.id)
            if not self._bank_service.check_is_bank_account_belongs_to_user(int(context.args[0]), user):
                update.message.reply_text(NO_RIGHTS_FOR_BANK_REPLY)
                return
            amount = self._bank_service.get_bank_account_by_account_id(int(context.args[0])).amount
            if amount is None:
                amount = 0
            update.message.reply_text(str(amount))

    @phone_required
    def card_amount(self, update: Update, context: CallbackContext) -> None:
        if len(context.args) != 1:
            update.message.reply_text(INVALID_ARGUMENTS_CARD_AMOUNT_REPLY)
        else:
            user = self.tguser_service.get_user(update.effective_user.id)
            bank_account = self._bank_service.get_card_by_number(int(context.args[0])).bank_account
            if not self._bank_service.check_is_bank_account_belongs_to_user(bank_account.id, user):
                update.message.reply_text(NO_RIGHTS_FOR_BANK_REPLY)
                return
            update.message.reply_text(str(bank_account.amount))

    @phone_required
    def transfer_amount(self, update: Update, context: CallbackContext) -> None:
        if not self.check_validity(' '.join(context.args)):
            update.message.reply_text(INVALID_ARGUMENTS_TRANSFER_AMOUNT_REPLY)
            return
        user = self.tguser_service.get_user(update.effective_user.id)
        result_message = self.get_transfer_message(user,
                                                   context.args[0], context.args[1], context.args[2], int(context.args[3]))
        update.message.reply_text(result_message)

    def get_transfer_message(self, user, user_account_id: str, command_type: str, identificator: str, money_sum: int) -> str:
        user_account = self._bank_service.get_bank_account_by_account_id(user_account_id)
        get_bank_account_command = self.commands_dictionary[command_type]
        target_account = get_bank_account_command(identificator)
        if target_account is None:
            return NO_SUCH_ACCOUNT_REPLY
        target_user = target_account.tg_user
        if not self._fav_service .check_is_favourite_tguser(user, target_user):
            return USER_NOT_FAVOURITE_REPLY
        transaction_result = self._bank_service.transfer_money(user_account, target_account, money_sum)
        if transaction_result is False:
            return "Failed transaction"
        basic_message = MONEY_TRANSFERED_REPLY
        current_amount = str(self._bank_service.get_bank_account_by_account_id(int(user_account_id)).amount)
        return basic_message.format(account_id=user_account_id, amount=current_amount)

    @phone_required
    def get_interactions(self, update: Update, context: CallbackContext) -> None:
        interacted_with_usernames = self._bank_service.get_usernames_from_interactions(tg_id=update.effective_user.id)
        interacted_with_message_text = self.get_interactions_relpy_text(interacted_with_usernames)
        update.message.reply_text(interacted_with_message_text)

    @phone_required
    def get_transactions(self, update: Update, context: CallbackContext) -> None:
        if len(context.args) != 1:
            update.message.reply_text(INVALID_ARGUMENTS_GET_TRANSACTIONS_REPLY)
            return
        user = self.tguser_service.get_user(update.effective_user.id)
        if not self._bank_service.check_is_bank_account_belongs_to_user(int(context.args[0]), user):
            update.message.reply_text(NO_RIGHTS_FOR_BANK_REPLY)
            return
        money_transactions = self._bank_service.get_money_transactions_for_account(user.telegram_id, context.args[0])
        money_transactions_message_text = self.get_transactions_relpy_text(context.args[0], money_transactions)
        update.message.reply_text(money_transactions_message_text)

    def get_interactions_relpy_text(self, interacted_usernames: List[str]) -> str:
        interacted_with_message_text = GET_INTERACTIONS_START_REPLY
        for index, interacted_with_username in enumerate(interacted_usernames):
            interacted_with_message_text += f"{index + 1}. @{interacted_with_username}\n"
        return interacted_with_message_text

    def get_transactions_relpy_text(self, account_id_str: str, money_transactions: List[MoneyTransactionIn]) -> str:
        money_transactions_message_text = GET_TRANSACTIONS_START_REPLY.format(account_id=account_id_str)
        for index, money_transaction in enumerate(money_transactions):
            money_transactions_message_text += (
                f"{index + 1}. Sender: @{money_transaction.sender.tg_user.username}. "
                f"account_id: {money_transaction.sender.id} "
                f"Receiver: @{money_transaction.receiver.tg_user.username} "
                f"account_id: {money_transaction.receiver.id} "
                f"Amount: {money_transaction.amount}.\n"
            )
        return money_transactions_message_text
