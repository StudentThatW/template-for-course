from django.contrib import admin

from app.internal.bank_account_and_card.db.models import BankAccount, Card, MoneyTransaction


@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    list_display = ("id", "amount")
    pass


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    list_display = ("number", "bank_account")
    pass


@admin.register(MoneyTransaction)
class MoneyTransactionAdmin(admin.ModelAdmin):
    list_display = ("sender", "receiver", "amount", "date")
    pass
