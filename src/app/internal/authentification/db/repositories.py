from abc import ABC, abstractmethod
from datetime import datetime, timedelta
from typing import Any, Dict, Optional, Tuple

import jwt
from django.utils.timezone import now

from app.internal.authentification.db.models import IssuedToken
from app.internal.tg_users.domain.entities import TgUserIn, TgUserOut
from config.settings import JWT_ALGORITHM, JWT_SECRET

ACCESS_TOKEN_EXPIRES_INTERVAl = timedelta(minutes=5)
REFRESH_TOKEN_EXPIRES_INTERVAl = timedelta(days=30)


class IIssuedTokenRepository(ABC):
    @abstractmethod
    def generate_access_token(self, tg_user: TgUserIn) -> str:
        ...

    @abstractmethod
    def generate_refresh_token(self, tg_user: TgUserIn) -> str:
        ...

    @abstractmethod
    def get_refresh_token(self, jti: str) -> IssuedToken | None:
        ...

    @abstractmethod
    def is_refresh_token_expired(self, refresh_token: IssuedToken) -> bool:
        ...

    @abstractmethod
    def revoke_tokens(self, tg_user: TgUserIn) -> None:
        ...

    @abstractmethod
    def decode_token(self, token: str) -> Dict[str, Any]:
        ...

    @abstractmethod
    def revoke_refresh_token(self, refresh_token: IssuedToken):
        ...


class IssuedTokenRepository(IIssuedTokenRepository):
    def generate_access_token(self, tg_user: TgUserIn) -> str:
        expires_date = (datetime.utcnow() + ACCESS_TOKEN_EXPIRES_INTERVAl).timestamp()
        payload = {"type": "access", "tguser_id": str(tg_user.telegram_id), "expires": expires_date}
        return jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)

    def generate_refresh_token(self, tg_user: TgUserIn) -> str:
        current_date = datetime.utcnow()
        expires_date = (current_date + REFRESH_TOKEN_EXPIRES_INTERVAl).timestamp()
        payload = {"type": "refresh", "tguser_id": str(tg_user.telegram_id), "expires": expires_date}
        refresh_token = jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)
        IssuedToken.objects.create(jti=refresh_token, user=tg_user, created_at=current_date)
        return refresh_token

    def get_refresh_token(self, jti: str) -> IssuedToken | None:
        return IssuedToken.objects.select_related("user").filter(jti=jti).first()

    def is_refresh_token_expired(self, refresh_token: IssuedToken) -> bool:
        return (refresh_token.created_at + REFRESH_TOKEN_EXPIRES_INTERVAl).timestamp() < datetime.now().timestamp()

    def revoke_tokens(self, tg_user: TgUserIn) -> None:
        IssuedToken.objects.filter(user=tg_user).update(revoked=True)

    def decode_token(self, token: str) -> Dict[str, Any]:
        return jwt.decode(token, key=JWT_SECRET, algorithms=[JWT_ALGORITHM])

    def revoke_refresh_token(self, refresh_token: IssuedToken):
        refresh_token.revoked = True
        refresh_token.save()
