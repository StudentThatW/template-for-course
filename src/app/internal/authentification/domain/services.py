from typing import Any, Dict, Optional, Tuple

from app.internal.authentification.db.repositories import IIssuedTokenRepository
from app.internal.authentification.domain.entities import IssuedTokenIn, IssuedTokenOut
from app.internal.tg_users.domain.entities import TgUserIn


class IssuedTokenService:
    def __init__(self, issued_token_repo: IIssuedTokenRepository):
        self._issued_token_repo = issued_token_repo

    def generate_access_token(self, tg_user: TgUserIn) -> str:
        return self._issued_token_repo.generate_access_token(tg_user)

    def generate_refresh_token(self, tg_user: TgUserIn) -> str:
        return self._issued_token_repo.generate_refresh_token(tg_user)

    def generate_access_and_refresh_tokens(self, tg_user: TgUserIn) -> Tuple[str, str]:
        access_token = self.generate_access_token(tg_user)
        refresh_token = self.generate_refresh_token(tg_user)
        return access_token, refresh_token

    def get_refresh_token(self, jti: str) -> IssuedTokenOut | None:
        return self._issued_token_repo.get_refresh_token(jti)

    def is_refresh_token_expired(self, refresh_token: IssuedTokenIn) -> bool:
        return self._issued_token_repo.is_refresh_token_expired(refresh_token)

    def revoke_tokens(self, tg_user: TgUserIn) -> None:
        self._issued_token_repo.revoke_tokens(tg_user)

    def decode_token(self, token: str) -> Dict[str, Any]:
        return self._issued_token_repo.decode_token(token)

    def revoke_refresh_token(self, refresh_token: IssuedTokenIn):
        self._issued_token_repo.revoke_refresh_token(refresh_token)
