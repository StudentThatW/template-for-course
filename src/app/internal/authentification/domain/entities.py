from ninja import Schema
from ninja.orm import create_schema

from app.internal.authentification.db.models import IssuedToken

IssuedTokenSchema = create_schema(IssuedToken)


class IssuedTokenOut(IssuedTokenSchema):
    ...


class IssuedTokenIn(IssuedTokenSchema):
    ...


class AuthSchema(Schema):
    login: str
    password: str


class TokenSchema(Schema):
    access_token: str
    refresh_token: str


class JtiSchema(Schema):
    jti: str
