from typing import List

from django.db.utils import IntegrityError
from ninja import Body
from ninja.errors import HttpError

from app.internal.authentification.domain.entities import AuthSchema, JtiSchema, TokenSchema
from app.internal.authentification.domain.services import IssuedTokenService
from app.internal.responses.error_response import ErrorResponse
from app.internal.responses.success_response import SuccessResponse
from app.internal.tg_users.domain.services import TgUserService


class IssuedTokenHandlers:
    def __init__(self, token_service: IssuedTokenService, _tguser_service: TgUserService):
        self._token_service = token_service
        self.tguser_service = _tguser_service

    def post_login(self, request, auth_schema: AuthSchema = Body(...)):
        login = auth_schema.login
        password = auth_schema.password

        user, message = self.tguser_service.check_and_get_user(login)
        if user is None:
            return 401, ErrorResponse(message=message)

        password_hash = self.tguser_service.get_hash(password)
        expected_hash = bytes(user.password_hash)
        if expected_hash != password_hash:
            return 401, ErrorResponse(message="Incorrect password")

        self._token_service.revoke_tokens(user)
        access_token, refresh_token = self._token_service.generate_access_and_refresh_tokens(user)
        return 201, TokenSchema(access_token=access_token, refresh_token=refresh_token)

    def post_revoke_token(self, request, jti_sch: JtiSchema = Body(...)):
        jti = jti_sch.jti
        refresh_token = self._token_service.get_refresh_token(jti)
        if not refresh_token:
            return 404, ErrorResponse(message="Token not found")
        if refresh_token.revoked:
            return 400, ErrorResponse(message="Token revoked")
        if self._token_service.is_refresh_token_expired(refresh_token):
            self._token_service.revoke_refresh_token(refresh_token)
            return 404, ErrorResponse(message="Token expired")
        self._token_service.revoke_refresh_token(refresh_token)
        access_token, refresh_token = self._token_service.generate_access_and_refresh_tokens(refresh_token.user)
        return 201, TokenSchema(access_token=access_token, refresh_token=refresh_token)
