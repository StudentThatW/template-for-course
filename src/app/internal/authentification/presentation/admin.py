from django.contrib import admin

from app.internal.authentification.db.models import IssuedToken


@admin.register(IssuedToken)
class IssuedTokenAdmin(admin.ModelAdmin):
    pass
