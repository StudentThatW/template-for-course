from typing import List

from ninja import NinjaAPI, Router
from ninja.errors import HttpError

from app.internal.authentification.domain.entities import AuthSchema, TokenSchema
from app.internal.authentification.presentation.handlers import IssuedTokenHandlers
from app.internal.responses.error_response import ErrorResponse


def add_iss_router(api: NinjaAPI, iss_handlers: IssuedTokenHandlers):
    iss_router = get_iss_router(iss_handlers)
    api.add_router("/authentication", iss_router)


def get_iss_router(iss_handlers: IssuedTokenHandlers):
    router = Router(tags=["authentication"])

    router.add_api_operation("login", ["POST"], iss_handlers.post_login, response={201: TokenSchema, 401: ErrorResponse},)
    router.add_api_operation("refresh", ["POST"], iss_handlers.post_revoke_token, response={201: TokenSchema, 400: ErrorResponse, 404: ErrorResponse})
    return router
