from ninja import NinjaAPI

from app.internal.authentification.db.repositories import IssuedTokenRepository
from app.internal.authentification.domain.services import IssuedTokenService
from app.internal.authentification.presentation.handlers import IssuedTokenHandlers
from app.internal.authentification.presentation.routers import add_iss_router
from app.internal.tg_users.db.repositories import TgUserRepository
from app.internal.tg_users.domain.services import TgUserService


def add_token_api(api: NinjaAPI):
    iss_repo = IssuedTokenRepository()
    iss_service = IssuedTokenService(iss_repo)

    tguser_repo = TgUserRepository()
    tguser_service = TgUserService(tguser_repo)

    iss_handler = IssuedTokenHandlers(iss_service, tguser_service)
    add_iss_router(api, iss_handler)
