import enum

from telegram import Bot, Update
from telegram.ext import CommandHandler, Dispatcher, Updater

from app.internal.bank_account_and_card.db.repositories import BankAccountCardRepository
from app.internal.bank_account_and_card.domain.services import BankAccountCardService
from app.internal.bank_account_and_card.presentation.bot_handlers import BankAccountCardBotHandlers
from app.internal.favourite_users.db.repositories import FavouriteTgUsersRepository
from app.internal.favourite_users.domain.services import FavouriteTgUsersService
from app.internal.favourite_users.presentation.bot_handlers import FavouriteTgUsersBotHandlers
from app.internal.tg_users.db.repositories import TgUserRepository
from app.internal.tg_users.domain.services import TgUserService
from app.internal.tg_users.presentation.bot_handlers import TgUsersBotHandlers
from config.settings import TG_BOT_TOKEN


class Commands(enum.Enum):
    start = "start"
    set_phone = "set_phone"
    me = "me"


def get_handlers():
    tguser_repo = TgUserRepository()
    tguser_service = TgUserService(tguser_repo)
    tguser_handler = TgUsersBotHandlers(tguser_service)

    fav_user_repo = FavouriteTgUsersRepository()
    fav_user_service = FavouriteTgUsersService(fav_user_repo)

    fav_user_handler = FavouriteTgUsersBotHandlers(fav_user_service, tguser_service)

    bank_repo = BankAccountCardRepository()
    bank_service = BankAccountCardService(bank_repo)
    bank_handler = BankAccountCardBotHandlers(bank_service, fav_user_service, tguser_service)
    return tguser_handler, fav_user_handler, bank_handler


def start_bot():
    updater = Updater(TG_BOT_TOKEN, use_context=True)
    t_handler, f_handler, b_handler = get_handlers()

    updater.dispatcher.add_handler(CommandHandler("start", t_handler.start))
    updater.dispatcher.add_handler(CommandHandler("set_phone", t_handler.set_phone))
    updater.dispatcher.add_handler(CommandHandler("me", t_handler.me))
    updater.dispatcher.add_handler(CommandHandler("bank_account", b_handler.get_account_amount))
    updater.dispatcher.add_handler(CommandHandler("card", b_handler.card_amount))
    updater.dispatcher.add_handler(CommandHandler("add_favourite", f_handler.add_favorite))
    updater.dispatcher.add_handler(CommandHandler("get_favorite", f_handler.get_favorite))
    updater.dispatcher.add_handler(CommandHandler("remove_favorite", f_handler.remove_favorite))
    updater.dispatcher.add_handler(CommandHandler("transfer_amount", b_handler.transfer_amount))
    updater.dispatcher.add_handler(CommandHandler("get_interactions", b_handler.get_interactions))
    updater.dispatcher.add_handler(CommandHandler("get_transactions", b_handler.get_transactions))
    updater.dispatcher.add_handler(CommandHandler("set_password", t_handler.set_password))

    updater.start_polling()
    updater.idle()


def run_bot():
    updater = Updater(token=TG_BOT_TOKEN, use_context=True)

    t_handler, f_handler, b_handler = get_handlers()

    updater.dispatcher.add_handler(CommandHandler("start", t_handler.start))
    updater.dispatcher.add_handler(CommandHandler("set_phone", t_handler.set_phone))
    updater.dispatcher.add_handler(CommandHandler("me", t_handler.me))
    updater.dispatcher.add_handler(CommandHandler("bank_account", b_handler.get_account_amount))
    updater.dispatcher.add_handler(CommandHandler("card", b_handler.card_amount))
    updater.dispatcher.add_handler(CommandHandler("add_favourite", f_handler.add_favorite))
    updater.dispatcher.add_handler(CommandHandler("get_favorite", f_handler.get_favorite))
    updater.dispatcher.add_handler(CommandHandler("remove_favorite", f_handler.remove_favorite))
    updater.dispatcher.add_handler(CommandHandler("transfer_amount", b_handler.transfer_amount))
    updater.dispatcher.add_handler(CommandHandler("get_interactions", b_handler.get_interactions))
    updater.dispatcher.add_handler(CommandHandler("get_transactions", b_handler.get_transactions))
    updater.dispatcher.add_handler(CommandHandler("set_password", t_handler.set_password))

    updater.start_webhook(
        listen="0.0.0.0",
        port='8443',
        url_path=TG_BOT_TOKEN,
        webhook_url=f"https://charrau.backend22.2tapp.cc/{TG_BOT_TOKEN}",
    )
    updater.idle()
