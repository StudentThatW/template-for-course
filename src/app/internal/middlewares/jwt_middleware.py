from datetime import datetime
from typing import Optional

from django.http import HttpRequest, HttpResponse, JsonResponse

from app.internal.authentification.db.repositories import IssuedTokenRepository
from app.internal.authentification.domain.services import IssuedTokenService


class JWTAuthMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> HttpResponse:
        if "api/" not in request.path:
            return self.get_response(request)

        iss_repo = IssuedTokenRepository()
        iss_service = IssuedTokenService(iss_repo)
        token = self._get_bearer_token(request)
        error = None
        if token is None:
            error = {"success": False, "error": "Invalid request"}
            return JsonResponse(error, status=403)
        payload = iss_service.decode_token(token)

        if not payload:
            error = {"success": False, "error": "Unauthorized"}
        if payload and payload.get("type") != "access":
            error = {"success": False, "error": "Invalid token type"}

        exp = float(payload.get("expires"))
        if payload and exp < (datetime.now()).timestamp():
            error = {"success": False, "error": "Token expired"}
        if error is not None:
            return JsonResponse(error, status=403)
        return self.get_response(request)

    @staticmethod
    def _get_bearer_token(request: HttpRequest) -> Optional[str]:
        parameters = request.headers.get("Authorization", "").split()
        if len(parameters) != 2 or parameters[0].lower() != "bearer":
            return None
        return parameters[1]
