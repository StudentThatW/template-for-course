from django.core.management.base import BaseCommand

from app.internal.bot import run_bot, start_bot
from config.settings import TG_BOT_TOKEN


class Command(BaseCommand):
    help = "Start bot"

    def handle(self, *args, **options):
        run_bot()
