from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from app.internal.authentification.presentation.admin import IssuedTokenAdmin
from app.internal.bank_account_and_card.presentation.admin import BankAccountAdmin, CardAdmin, MoneyTransactionAdmin
from app.internal.favourite_users.presentation.admin import FavouriteTgUserAdmin
from app.internal.tg_users.presentation.admin import TgUserAdmin
from app.models import AdminUser


@admin.register(AdminUser)
class AdminUserAdmin(UserAdmin):
    pass


admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
